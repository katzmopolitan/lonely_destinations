desc "Import destinations"
namespace :data do

  task import_destinations: :environment  do
    file_url = ENV["DESTINATIONS_FILE_URL"]
    raise "DESTINATIONS_FILE_URL must be set" unless file_url
    DestinationsWorker.new.perform(file_url)
  end

  task import_taxonomy: :environment  do
    file_url = ENV["TAXONOMY_FILE_URL"]
    raise "TAXONOMY_FILE_URL must be set" unless file_url
    TaxonomyWorker.new.perform(file_url)
  end

end

