class CreateDestinations < ActiveRecord::Migration
  def change
    create_table :destinations do |t|
      t.integer :atlast_id
      t.string :asset_id
      t.string :title
      t.text :content

      t.timestamps
    end
    add_index :destinations, :atlast_id, unique: true
  end
end
