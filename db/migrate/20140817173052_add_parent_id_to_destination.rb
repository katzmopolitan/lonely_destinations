class AddParentIdToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :parent_atlas_id, :integer
  end
end
