# Assignment

Lonely Planet Coding Exercise

We have two (admittedly not very clean) .xml files from our legacy CMS system - taxonomy.xml holds the information about how destinations are related to each other and destinations.xml holds the actual text content for each destination.

We would like you to create a batch processor that takes these input files and produces an .html file (based on the output template given with this test) for each destination. Each generated web page must have:

1. Some destination text content. Use your own discretion to decide how much information to display on the destination page.

2. Navigation that allows the user to browse to destinations that are higher in the taxonomy. For example, Beijing should have a link to China.

3. Navigation that allows the user to browse to destinations that are lower in the taxonomy. For example, China should have a link to Beijing.

The batch processor should take the location of the two input files and the output directory as parameters.

These sample input files contain only a small subset of destinations.  We will test your software on the full Lonely Planet dataset, which currently consists of almost 30,000 destinations.

To submit your code, either ZIP it up and email it to the address below, or give us a link to your github repo.

When we receive your project the code will be:

1. Built and run against the dataset supplied.

1. Evaluated based on coding style and design choices in all of these areas:

    1. Readability.

    1. Simplicity.

    1. Extensibility.

    1. Reliability.

    1. Performance.

Feedback at this stage will be provided to all candidates if requested.

#Setup

## Perequisites

- rubygems
- ruby

## Get the code

`git clone git@github.com:ilyakatz/lonely_destinations.git`

## Install gems

`gem install bundler`

`bundle install`

or

`bundle install --without production` (if you dont' want to install postgre)

## Setup database

`createuser lonely_destinations --createdb`

`rake db:create db:migrate`

## Run tests

`rake db:migrate RAILS_ENV=test`

`rspec`

# Data import

`export DESTINATIONS_FILE_URL=https://dl.dropboxusercontent.com/u/5405974/lonely/destinations.xml`

or

`export DESTINATIONS_FILE_URL=~/destinations.xml`

`rake data:import_destinations`

`export TAXONOMY_FILE_URL=https://dl.dropboxusercontent.com/u/5405974/lonely/taxonomy.xml`

or

`export TAXONOMY_FILE_URL=~/taxonomy.xml`

`rake data:import_taxonomy`

# Check it out

`rails s`

Open localhost:3000 in the browser

A copy of this application is available at [http://lonely-destinations.herokuapp.com/](http://lonely-destinations.herokuapp.com/)
