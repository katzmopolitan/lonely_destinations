require 'sidekiq/web'

RailsTemplate::Application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  resources :destinations, only: [:index, :show]
  root "destinations#index"
end
