# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :destination do
    atlast_id { Faker::Number.number(5) }
    asset_id "MyString"
    title "Sample Destination"
    content "MyText"
  end
end
