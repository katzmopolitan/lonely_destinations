require 'spec_helper'

describe Destination do

  it "should show destinations without parents" do
    d = create(:destination)
    child = create(:destination, parent_atlas_id: d.atlast_id)
    Destination.top_level.count.should eq 1
    Destination.top_level.should include d
  end

  it "should show children correctly" do
    d = create(:destination)
    child = create(:destination, parent_atlas_id: d.atlast_id)
    d.children.count.should eq 1
    d.children.should include child
  end

end
