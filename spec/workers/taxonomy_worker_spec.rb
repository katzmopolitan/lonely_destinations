require 'spec_helper'

describe TaxonomyWorker do

  it "should map parent and children nodes correctly" do
    file = "#{Rails.root}/spec/fixtures/taxonomy.xml"
    TaxonomyWorker.new.perform(file)
    destinations = Destination.all
    expect(destinations.count).to eq 2
    parent = Destination.find_by(atlast_id: 355064)
    child = Destination.find_by(atlast_id: 355633)
    expect(parent.children.count).to eq 1
    expect(parent.children).to include child
  end

  it "should not have a parent for top level element" do
    file = "#{Rails.root}/spec/fixtures/taxonomy.xml"
    TaxonomyWorker.new.perform(file)
    top = Destination.first
    expect(top.parent_atlas_id).to be nil
  end

  it "should be rerunnable" do
    file = "#{Rails.root}/spec/fixtures/taxonomy.xml"
    TaxonomyWorker.new.perform(file)
    expect { TaxonomyWorker.new.perform(file) }.to_not raise_error
  end

end
