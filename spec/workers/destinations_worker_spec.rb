require 'spec_helper'

describe DestinationsWorker do

  it "should process file properly" do
    file = "#{Rails.root}/spec/fixtures/destinations.xml"
    DestinationsWorker.new.perform(file)
    destinations = Destination.all
    destinations.count.should eq 2
    destinations.first.title.should eq "Africa"
    destinations.second.title.should eq "Swaziland"
  end

  it "should be rerunnable" do
    file = "#{Rails.root}/spec/fixtures/destinations.xml"
    DestinationsWorker.new.perform(file)
    expect { DestinationsWorker.new.perform(file) }.to_not raise_error
  end
end
