require 'spec_helper'

describe ApplicationHelper do
  include ApplicationHelper
  include ActionView::Helpers::UrlHelper

  describe "#next_section" do

    it "should product parameter for the root section" do
      stub(:params).and_return({})
      next_section("africa").should eq "africa"
    end

    it "should return hyphenated section parameter" do
      stub(:params).and_return(
        {
          "section" => "africa-zimbabwe-history"
        }
      )
      next_section("prehistoric").should eq "africa-zimbabwe-history-prehistoric"
    end

  end

  describe "#navigation" do

    it "should show link to the next destination" do
      stub(:params).and_return({})
      destination = build(:destination)
      expected_url = "http://some/url"
      self.should_receive(:destination_url).and_return(expected_url)
      navigation("history", destination).should eq link_to("History", expected_url)
    end

  end
end
