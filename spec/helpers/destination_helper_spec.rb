require 'spec_helper'

describe DestinationHelper do
  include DestinationHelper
  include ActionView::Helpers::UrlHelper

  describe "#destination_title" do

    it "should show default title" do
      self.should_receive(:content_for).with(:destination_title).and_return(nil)
      destination_title.should eq link_to "Lonely Planet", root_path
    end

    it "should include destnation name in the title" do
      self.should_receive(:content_for).with(:destination_title).exactly(2).times.and_return("Hong Kong")
      destination_title.should match "Hong Kong"
    end

  end
end
