require 'spec_helper'

describe "destinations/_content.html.erb", type: :view do

  it "should display a single line of content" do
    view.stub(:content).and_return("line 1")
    render
    rendered.strip.should eq "line 1"
  end

  it "should display a multi line line" do
    view.stub(:content).and_return(["line 1", "line 2"])
    render
    rendered.strip.should eq "<p>line 1</p><p>line 2</p>"
  end

  it "should display navigation links" do
    view.stub(:content).and_return({"section" => "Content"})
    view.stub(:destination).and_return(create(:destination))
    render
    response.should render_template(partial: "_navigation")
  end

end
