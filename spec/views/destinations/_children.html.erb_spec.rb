require 'spec_helper'

describe "destinations/_children.html.erb", type: :view do

  it "should not show anything if current destination doesn't have any children" do
    d = create(:destination)
    view.stub(:destination).and_return(d)
    render
    rendered.should eq ""
  end

  it "should show children" do
    d = create(:destination)
    child = create(:destination, parent_atlas_id: d.atlast_id)
    view.stub(:destination).and_return(d)
    render
    rendered.should include "Destinations"
    rendered.should include child.title
  end

end
