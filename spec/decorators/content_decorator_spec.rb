require "spec_helper"

describe ContentDecorator do

  describe "#readable?" do

    it "should indicate that the current content is leafe node (human readable content)" do
      c = ContentDecorator.new("This is some string")
      c.should be_readable
    end

    it "should indicate that the current content is not human readable content" do
      c = ContentDecorator.new({section: "This is some string"})
      c.should_not be_readable
    end

  end

  describe "#section?" do

    it "should indicate that the current content is a section with nested content" do
      c = ContentDecorator.new({section: "This is some content"})
      c.should be_section
    end

    it "should not indicate that the content is section if this is the leaf node with text" do
      c = ContentDecorator.new("This is some content")
      c.should_not be_section
    end
  end

  describe "#multiple_content" do

    it "should indicate that the current content is a collectoin of text" do
      c = ContentDecorator.new(["Line 1", "Line 2"])
      c.should be_multiple_content
    end

    it "should not indicate multi line content for leaf node with text" do
      c = ContentDecorator.new("Line 1")
      c.should_not be_multiple_content
    end

  end

  describe "#text" do

    it "should just show the content if the current section is leaf node with content" do
      c = ContentDecorator.new("This is some text")
      c.text.should eq "This is some text"
    end

    it "should concatinate sections if the current content is an array of text" do
      c = ContentDecorator.new(["Line 1", "Line 2"])
      c.text.should eq "<p>Line 1</p><p>Line 2</p>"
    end

  end
end
