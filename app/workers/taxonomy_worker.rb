require 'open-uri'

class TaxonomyWorker
  include Sidekiq::Worker

  #taxonomy_file = "/Users/katzmopolitan/Desktop/lonely_planet_coding_exercise_final/taxonomy.xml"
  def perform(taxonomy_file)
    assign_taxononomy(taxonomy_file)
  end

  private

  def assign_taxononomy(taxonomy_file)
    json = Hash.from_xml(open(taxonomy_file))
    world = json["taxonomies"]["taxonomy"]

    process_taxonomy_node(world["node"], nil)
  end

  def process_taxonomy_node(node, parent)
    if node.nil?
      return
    elsif node.is_a?(Hash)
      atlas_id = node["atlas_node_id"]
      persist_node(node, atlas_id, parent)
      process_taxonomy_node(node["node"], atlas_id)
    else
      node.each do |n|
        process_taxonomy_node(n, parent)
      end
    end
  end

  def persist_node(node, atlas_id, parent_id)
    name = node["node_name"]
    puts "#{name}, parent: #{parent_id}"
    destination = Destination.find_by(atlast_id: atlas_id)

    unless destination
      destination = Destination.create(atlast_id: atlas_id, title: name)
    end

    destination.update_attribute(:parent_atlas_id, parent_id) if parent_id
  end

end

