require 'open-uri'

class DestinationsWorker
  include Sidekiq::Worker

  def perform(destinations_file_path)
    process_destinations(destinations_file_path)
  end

  private

  def process_destinations(destinations_file_path)
    json = Hash.from_xml(open(destinations_file_path))

    destination_nodes(json).each do |d|
      persist_node(d)
    end
  end

  def destination_nodes(json)
    json["destinations"]["destination"]
  end

  def persist_node(node)
    atlas_id, asset_id, title, title_ascii =
      node["atlas_id"].to_i, node["asset_id"], node["title"], node["title_ascii"]
    content = node.select {|k,v| ["atlas_id", "asset_id", "title", "title_ascii"].exclude?(k) }

    destination = Destination.new(atlast_id: atlas_id,
                                     asset_id: asset_id,
                                     title: title,
                                     content: content.to_json)

    destination.save if destination.valid?

  end
end
