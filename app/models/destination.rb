class Destination < ActiveRecord::Base

  validates :atlast_id, uniqueness: true, presence: true

  scope :top_level, -> { where("parent_atlas_id is null") }

  def children
    Destination.where(parent_atlas_id: self.atlast_id)
  end
end
