module ApplicationHelper

  def navigation(title, destination)
    destination_path  = destination_url(destination, section: next_section(title) )
    link_to(title.humanize, destination_path)
  end

  def next_section(title)
    if params["section"]
      "#{params["section"]}-#{title}"
    else
      title
    end
  end

end
