module DestinationHelper
  def destination_title
    prefix = link_to "Lonely Planet", root_path
    if content_for(:destination_title).present?
      "#{prefix}: #{content_for(:destination_title)}".html_safe
    else
      prefix
    end
  end
end
