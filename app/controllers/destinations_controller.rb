class DestinationsController < ApplicationController

  def index
    @destinations = Destination.top_level.select("id, title")
  end

  def show
    @destination = Destination.find(params[:id])
    @content = content
  end

  private

  def content
    if params["section"]
      sections = params["section"].split("-")
      content = JSON.parse(@destination.content)
      sections.inject(content,:[])
    else
      content = JSON.parse(@destination.content)
    end
  end
end
