class ContentDecorator

  def initialize(content)
    @content = content
  end

  def readable?
    @content.is_a?(String)
  end

  def section?
    @content.is_a?(Hash)
  end

  def multiple_content?
    @content.is_a?(Array)
  end

  def text

    if readable?
      @content
    elsif multiple_content?
      text = ""
      @content.each do |c|
        text += "<p>#{c}</p>"
      end
      text
    end

  end

end
